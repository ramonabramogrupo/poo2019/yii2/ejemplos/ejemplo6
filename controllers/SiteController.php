<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud() {
        return $this->render("gestion");
    }
    
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 sin Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
     public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach' or nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"G_avg(edad)  (σ_(nomequipo='Banesto')(ciclista)",
        ]);
    }
}
